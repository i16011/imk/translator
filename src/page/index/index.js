import React, { Component } from 'react'
import SearchBox from './searchbox';
import NavbarMan from '../../parts/navbar';
import ResultBox from './resultbox';
import { inject, observer } from 'mobx-react';
class Index extends Component {
  render() {
    const {uistore} = this.props;
    const {results} = uistore; 
    return (<React.Fragment>
      <NavbarMan/>
      <div className="container">
        <SearchBox/>
      </div>
      <div style={{backgroundColor:"rgba(0,0,0,0.2)"}}>
        <div className="container">
          <div className={"row" + (results.length > 0?" py-5":"")}>
            {results.map((d, i)=>{
              return <ResultBox data={d} key={"res-" + i}/>;
            })}
          </div>
        </div>
      </div>
    </React.Fragment>);
  }
}
export default inject('uistore')(observer(Index));