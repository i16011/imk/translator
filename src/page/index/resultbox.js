import React, { Component } from 'react'
import {Card, CardBody, CardTitle, CardText} from 'mdbreact';
import {observer} from 'mobx-react'

class ResultBox extends Component {
  render() {
      const {data} = this.props;
    return (
    <div className={"w-100 m-2" + (data.opened?" z-depth-3":" z-depth-1")}>
      <Card className="z-depth-0 m-0">
        <CardBody>
            <CardTitle>{data.word}</CardTitle>
            <CardText>
                Tipe: <span className="badge badge-pill indigo">{data.type}</span>
            </CardText>

            <CardText>{data.desc}</CardText>


            <CardText className={"pt-3 " + (data.opened?"":" d-none")} style={{borderTop:"1px solid rgba(0,0,0,0.15)"}}>
                <h5>Contoh penggunaan pada konteks</h5>
                {data.examples.map((e,i)=>{
                    return <div key={"example-res-"+ i}>
                        <p>{e.context}</p>
                        <ul>
                            <li>{e.sentence}</li>
                        </ul>
                    </div>
                })}
            </CardText>

            
            <div className="text-right pt-3" style={{borderTop:"1px solid rgba(0,0,0,0.15)"}}>
                <a onClick={()=>{
                    data.opened=!data.opened
                }}>
                    {(data.opened?"Tutup Detil":"Lihat Detil")} <i className={"fa " + (data.opened?"fa-angle-double-up":"fa-angle-double-down")}/>
                </a>
            </div>
        </CardBody>
      </Card>
    </div>
    )
  }
}
export default observer(ResultBox);