import React, { Component } from 'react'
import {Card, Input, Button} from 'mdbreact';
import {observer, inject} from 'mobx-react'

import generate from "../../parts/data-generator";
class SearchBox extends Component {
  query=null;

  render() {
    const {uistore} = this.props;

    return (
      <div className="row align-items-center" style={{minHeight: uistore.searchHeight.get(), transition:"min-height 1s"}}>
        <div className="col-md-6 offset-md-3">
            <Card className="z-depth-0">
                <div className="text-center align-items-center px-4" style={{paddingTop: 20, paddingBottom:20}}>
                    <h3 className="text-center">Cari kata Indonesia atau Inggris</h3>
                    <Input type="text" onInput={(e)=>{this.query = e.target.value}}/>
                    <Button onClick={()=>{
                      uistore.searchHeight.set("20vh");
                      let data = generate.generate(this.query);
                      uistore.results.replace(data.map((e)=>{
                        e.open = false;
                        return e;
                      }));
                    }}>Translate</Button>
                </div>
            </Card>
        </div>
      </div>
    )
  }
}
export default inject('uistore')(observer(SearchBox));