import faker from 'faker';

export default {
    generate(query) {
        let datas = [];
        let randomnumber = this.getRandomInt(14,5);
        for (let index = 0; index < randomnumber; index++) {
            datas.push({
                word: query,
                type: this.randomPickType(),
                desc: faker.lorem.sentences(),
                examples: this.generateExample(this.getRandomInt(6,3))
            });
        }

        return datas;
    },

    generateExample(number) {
        let datas=[];
        for(let index = 0; index<number; index++){
            datas.push({   
                context: faker.lorem.words(),
                sentence: faker.lorem.sentence()
            });
        }
        return datas;
    },

    randomPickType(){
        let types = ['benda', 'sifat', 'kerja', 'konjugasi', 'Numeralia', 'Adverbia', 'partikel', 'depan', 'interjeksi'];
        return types[this.getRandomInt(types.length*2)%types.length];
    },

    getRandomInt(max, min) {
        min = min || 0;
        return Math.floor(Math.random() * Math.floor(max-min)) + min;
    }
}