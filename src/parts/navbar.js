import React, { Component } from 'react'
import {NavbarBrand, Navbar, NavbarNav, NavItem, NavLink} from 'mdbreact';
export default class NavbarMan extends Component {
  render() {
    return (
        <div className="container">
            <div className="row">
                <div className="col">
                <Navbar color="transparent" className="z-depth-0" expand="md" scrolling>
                    <NavbarBrand href="/"> <b>Translator</b> </NavbarBrand>
                    <NavbarNav right>
                        <NavItem>
                            <NavLink className="nav-link" to="/login">
                                Login 
                            </NavLink>
                        </NavItem>
                    </NavbarNav>
                </Navbar>
                </div>
            </div>
        </div>
    );
  }
}
