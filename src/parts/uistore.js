import {observable} from 'mobx';


export default class UIStore {
    user = observable({
        isLoggedIn: false,
        profile: {}
    });

    searchHeight= observable.box("80vh");

    results = observable([]);
}