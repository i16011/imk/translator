import React, { Component } from 'react';
import 'font-awesome/css/font-awesome.min.css';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import 'mdbreact/dist/css/mdb.css';
import { HashRouter, Route } from 'react-router-dom'
import {Switch} from 'react-router';

import UIStore from './parts/uistore';
import {Provider} from 'mobx-react';
/// PAGES
import Index from './page/index/index';

class App extends Component {
  render() {
    return (
      <HashRouter>
        <Provider uistore={new UIStore()}>
        <Switch>
          <Route exact path="/" component={Index}/>
        </Switch>
        </Provider>
      </HashRouter>
    );
  }
}

export default App;
